let collection = [];

// Write the queue functions below.
// Print
function print() {
  return collection;
}
// Enqueue
function enqueue(element) {
  collection[collection.length] = element;
  return  collection;
}
// Dequeue
function dequeue() {
  for (let i = 1; i < collection.length; i++)
      collection[i - 1] = collection[i];
      collection.length--;
      return collection;

};

// Front  //
function front() {
  newCollection =  collection[0];
  return newCollection;
}
// Size
function size() {
  return collection.length;
}
// isEmpty 
function isEmpty() {
  return collection.length == 0;
}

// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
    
};

